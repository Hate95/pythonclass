__author__ = 'student'

from myLogs import Loger
def sum(nr1,nr2):
    log = Loger("sum")
    log.info("return sum")
    return nr1+nr2

def first_read():
    log = Loger("first_read")
    log.info("Try read some number")
    try:
        nr1 = int(raw_input("Give first number : "))
        nr2 = int(raw_input("Give second number : "))
        log.info("Managed to read numbers")
        return sum(nr1,nr2)
    except ValueError as e:
        log.error("Invalid input ")
        print 'Invalid input'
        return 0
