__author__ = 'student'
from robots import  Robot
from first import first_read
def write_file(fileName):
    f = open(fileName,'w')
    with f:
        f.write('line1\n')
        f.write('line2\n')
        f.write('line3\n')
        f.write('line4\n')

def read_file(fileName):
    f = open(fileName,'r')
    with f:
        for line in f :
            print line[:-1]

if __name__ == '__main__':
    write_file("firstTextFile.txt")
    read_file("firstTextFile.txt")

    r = Robot("My robot","Red",20.5,30.5)
    r.prints()
    print r
    e = Robot()
    e.prints()

    print first_read()