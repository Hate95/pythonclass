from os.path import isfile
from myLogs import Logger
class MyFileIO():
    def __init__(self,fileName):
        self.__fileName = fileName
    def write(self,message):
        log = Logger("MyFileIO-Write")
        f= None
        if(isfile(self.__fileName)):
            log.info("file exist : "+ self.__fileName)
            f = open(self.__fileName,"a")
        else:
            log.info("file doesn't exist,create one with name :  "+ self.__fileName)
            f = open(self.__fileName,"w")
        with f:
            f.write(message+'\n')
            log.info("managed to write message with successfully")
            
    def read(self):
        #return a list with all lines
        log = Logger("MyFileIO-Read")
        if(isfile(self.__fileName)):
            l = []
            f = open(self.__fileName,"r+")
            with f:
                for line in f:
                    l.append(line)
            log.info("Managed to read line successfully")
            return l

