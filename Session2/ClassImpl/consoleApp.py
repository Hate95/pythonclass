from myLogs import Logger
from robot import Robot
from myException import MyException
from sum import Sum
from myFileIO import MyFileIO
class ConsoleApp():
    def init_a_robot(self):
        log = Logger("init_a_robot")
        try:
            log.info("Try to read from user data")
            name = str(raw_input("Give robot name : "))
            color = str(raw_input("Give robot color : "))
            height = float(raw_input("Give robot height : "))
            weight = float(raw_input("Give robot weight : "))
            log.info("Managed to read from user data")
            return Robot(name,color,height,weight)
        except ValueError:
            log.error("Invalid input from user")
            raise MyException("Invalid input ")
    
    def read_number(self):
        log = Logger("read_number")
        log.info("Try to read from user data")
        return int(raw_input("Give number : "))
    
    def run(self):
       #should catch here MyException and raise another ,lazy...
       r = self.init_a_robot()
       print r
       try:
           s = Sum(self.read_number(),self.read_number())
           print s.sum()
           
           myFileIo = MyFileIO("test.txt")
           myFileIo.write("line1")
           myFileIo.write("line2")
           
           l = myFileIo.read()
           for i in l:
               print i[:-1]
           
       except ValueError:
            log =Logger("run")
            log.error("Invalid input error")
            raise MyException("Invalid input ")
       
      
       
       