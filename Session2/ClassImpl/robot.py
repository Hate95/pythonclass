from myLogs import Logger
class Robot():
    #define default values for params on __init__ 
    def __init__(self,name="",color="",height=0.0,weight=0.0):
        self.__log = Logger("Robot")
        self.__log.info("Init a robot")
        self.__name = name
        self.__color = color
        self.__height = height
        self.__weight = weight
   
   #auto generated stuff 
    def get_name(self):
        return self.__name

    def get_color(self):
        return self.__color

    def get_height(self):
        return self.__height

    def get_weight(self):
        return self.__weight

    def set_name(self, value):
        self.__name = value

    def set_color(self, value):
        self.__color = value

    def set_height(self, value):
        self.__height = value

    def set_weight(self, value):
        self.__weight = value
    
    #meh
    name = property(get_name, set_name, None, None)
    color = property(get_color, set_color, None, None)
    height = property(get_height, set_height, None, None)
    weight = property(get_weight, set_weight, None, None)
    
    def __str__(self):
        #no good should use format ...
        self.__log.info("Attempting to return a string ")
        return "Name: "+self.__name +" Color: "+self.__color+\
               " Height: " + str(self.__height)+" Weight: " + str(self.__weight)
