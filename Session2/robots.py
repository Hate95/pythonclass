__author__ = 'student'

from myLogs import  Loger
class Robot:
    def __init__(self,name="",color="",height=0.0,weight=0.0):
        log = Loger("Robot")
        self.__name = name
        self.__color = color
        self.__height = height
        self.__weight = weight

    def prints(self):
        log = Loger("Robot")
        print " Name: "+self.__name
        print " Color: "+self.__color
        print " Height: " + str(self.__height)
        print " Weight: " + str(self.__weight)
        log.info("Managed to print a robot")

    def __str__(self):
        log = Loger("Robot")
        log.info("Try to return a robot")
        return "Name: "+self.__name +" Color: "+self.__color+" Height: " + str(self.__height)+" Weight: " + str(self.__weight)
