import random
def print_card(card):
    # print a card
    print "Card " + str(card['value']) + ' ' + card['color']
def compare_card(card, card1):
    # compare two cards after value
    if card['value'] == card1['value']:
        return None
    if card['value'] > card1['value']:
        return card
    return card1
 
__author__ = 'student'
colors = ['clubs', 'hearts', 'diamonds', 'spades']
deck = []
for i in range(2, 15):
    for  k in colors :
        card = {'color':k , 'value' : i}
        deck.append(card)
for card in deck:
    print_card(card)
x = 0
while(x < 20):
    print 'player 1 is picking a card'
    player1 = random.randint(0, 52)

    print 'player 2 is picking a card'
    player2 = random.randint(0, 50)

    print_card(deck[player1])
    print_card(deck[player2])
    
    card = compare_card(deck[player1], deck[player2])
    if(card == None):
        print 'cards are equal'
    elif card['value'] == deck[player1]['value']:
        print 'player 1 win' 
    else:
        print 'player 2 win'
    x+=1
