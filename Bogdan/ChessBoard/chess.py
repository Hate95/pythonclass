def chess_piece_init(name,color,position):
    #initialize a chess piece 
    return {'name':name,'color':color,'position':position}

def print_dict_chess_board(chess_board):
    #print a dictionary
        for chess_pieces in chess_board.values():
            print chess_pieces

def print_list_chess_board(chess_board):
    #print a list
        for chess_pieces in chess_board:
            print chess_pieces

def put_pice(chess_board,chess_piece):
    #puts a piece on dictionary or list board
    column = chess_piece['position'][0]
    line = chess_piece['position'][1]
    chess_board[column][line] = chess_piece
    return chess_board 

def dict_board():
    def dict_chess_board_init(chess_board):
        #initialize a dictionary to a chess board
        for column in range(8):
            chess_board[column] = [0,0,0,0,0,0,0,0]
        return chess_board
    
    def list_chess_board_init(chess_board):
        #initialize a list to a chess board
        for _ in range(8):
            chess_board.append([0,0,0,0,0,0,0,0])
        return chess_board
    
    #initialize chess board&piece
    dict_chess_board = dict_chess_board_init({})
    list_chess_board = list_chess_board_init([])

    chess_piece1 = chess_piece_init("turn","black",(0,0))    
    chess_piece2 = chess_piece_init("turn","black",[0,7])

    
    #dummy chess_board update
    #chess_board[chess_piece['position'][0]][chess_piece['position'][1]] = chess_piece 
    print "With dictionary :"
    dict_chess_board = put_pice(dict_chess_board, chess_piece1)    
    dict_chess_board = put_pice(dict_chess_board, chess_piece2)    
    print_dict_chess_board(dict_chess_board)
    
    print "\nWith lists :"
    list_chess_board = put_pice(list_chess_board, chess_piece1)    
    list_chess_board = put_pice(list_chess_board, chess_piece2)    
    print_list_chess_board(list_chess_board)
    
dict_board()
