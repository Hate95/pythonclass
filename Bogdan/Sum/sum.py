# Add the numbers smaller than a given number

def add_them_up(max):
    sum = 0
    numbers = range(max)
    for number in numbers:
        sum += number
    return sum


print add_them_up(4)

# Modify the function to add the number smaller or equal to the given number
# Modify the function to add only odd numbers in the sum

def generic_funct(nr, y=None, key=None):
    if(key == None):
        key = lambda x, y : True 
    if(y == None):
        y = nr
    sum = 0 
    for number in range(nr):
        if(key(number, y)):
            sum += number
    return sum
    
print generic_funct(4)
print generic_funct(4, y=2,key = lambda x,y : x<y)
print generic_funct(4, y=2,key = lambda x,y : x<=y)
print generic_funct(4,key = lambda x,y : x %2 ==0)