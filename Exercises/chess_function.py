#write a function that creates a chess piece given the name, color and position
def make_chess_piece(name, color, position):
    cp = {}
    cp["name"] = name
    cp["color"] = color
    cp["position"] = position
    print "Created ", color, name," at position ", position
    return cp

bishop = make_chess_piece("bishop","white",(1,1))
