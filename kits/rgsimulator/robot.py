from myLogs import Logger
from rgkit import rg
class Robot():
    #define default values for params on __init__
    def __init__(self,name="",color="",height=0.0,weight=0.0):
        self.__log = Logger("Robot")

    def act(self,game):
        if(rg.dist(self.location,rg.CENTER_POINT) == 0):
            self.__log.info("Robot is in center"+str(self.location)+" "+str(rg.CENTER_POINT))
            return ['suicide']
        x = ['move',rg.toward(self.location,rg.CENTER_POINT)]
        self.__log.info("Robot move to "+str(x))
        return x
