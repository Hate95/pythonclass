from rgkit import rg
from rgkit.game import Game

class Robot:
    def __init__(self):
        self.all_locs = [(x, y) for x in range(19) for y in range(19)]
        self.spawn = [loc for loc in self.all_locs if 'spawn' in rg.loc_types(loc)]
        self.last_location = ()
        self.last_move = ()
        self.last_enemy = ()
        self.enemy_hp = 0
    def act(self, game):
        adjent_locations = rg.locs_around(self.location)
        obstacle = [loc for loc in self.all_locs if 'obstacle' in rg.loc_types(loc)]
        team = [loc for loc in game.robots if game.robots[loc].player_id == self.player_id]
        enemy = [loc for loc in game.robots if game.robots[loc].player_id != self.player_id]
        
        if self.location in self.spawn:
            attack = self.attack(enemy, adjent_locations,game)
            print attack
            if attack == None:
                self.last_location = self.location
                self.last_move = rg.toward(self.location, rg.CENTER_POINT)
                return ['move', self.last_move]
            return attack 

        attack = self.attack(enemy, adjent_locations,game)
        print attack 
        if attack != None:
            return attack
        friend = self.close_friend(team)
        print friend
        if friend[1] <= 2:
            friend_in_spawn = self.friend_adjent_spawn(adjent_locations, team,obstacle)
            if friend_in_spawn != None:
                return friend_in_spawn
            return ['guard']
        return ['move', rg.toward(self.location, friend[0])]
  
    def friend_adjent_spawn(self,adjent_locaations,team,obstacle):
        team = list(team)
        for loc in adjent_locaations:
            if loc in team and loc in self.spawn:
                for loc1 in adjent_locaations:
                    if loc1 not in team and loc1 not in obstacle and loc1 not in self.spawn:
                        return ['move',rg.toward(self.location, loc1)]
        return None
                
        
    def close_friend(self, team):
        if len(list(team))==1:
            return [(),1][:]
        team = list(team)
        if self.location != team[0]:
            min = rg.wdist(self.location, team[0])
            next = team[0]
        else:
            min = rg.wdist(self.location, team[1])
            next = team[1]

        for loc in team:
            dist = rg.wdist(self.location, loc)
            if  loc != self.location and dist < min:
                next = loc
                min = dist
        return [next, min][:]

    def attack(self, enemy, adjent_locations,game):
        self.last_location = self.location
        for target in adjent_locations:
            if target in enemy:
                self.last_move = target
                self.last_enemy = target
                if self.hp <=20 and self.enemy_hp-game.robots[target].hp<11:
                   return  ['suicide']
                self.enemy_hp = game.robots[target].hp
                return ['attack', target]

        for next_target in adjent_locations:
            for target in rg.loc_types(next_target):
                if target in enemy:
                    self.last_move = next_target
                    self.last_enemy = target
                    if self.hp <=20 and self.enemy_hp-game.robots[target].hp<11:
                        return  ['suicide']
                    self.enemy_hp = game.robots[next_target].hp
                    return ['move', next_target]                    
        return None

    
