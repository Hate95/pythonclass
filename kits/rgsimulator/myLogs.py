import logging

class Logger:
    def __init__(self,Name = "root"):
        logging.basicConfig(level=logging.DEBUG,
                                format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                                datefmt='%m-%d %H:%M',
                                filename='logs.log',
                                filemode='w')
        self._log = logging.getLogger(Name)
    def error(self,msg):
        self._log.error(msg)
    def info(self,msg):
        self._log.info(msg)
    def debug(self,msg):
        self._log.debug(msg)
    def warining(self,msg):
        self._log.warning(msg)
    def critical(self,msg):
        self._log.critical(msg)